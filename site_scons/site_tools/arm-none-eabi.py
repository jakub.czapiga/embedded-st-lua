#!/usr/bin/env python
# coding=utf8

import os

from SCons.Tool import gcc
from SCons.Script import *
from SCons.Action import Action

_COMPILER_PREFIX = 'arm-none-eabi-'

compilers = ['gcc', 'cc']


def _check_tool_path(env):
    if 'ARM_GNU_DIR' not in os.environ:
        raise Exception(
            'ARM_GNU_DIR not set. Please specify in environment variables')

    if not os.path.isdir(os.environ['ARM_GNU_DIR']):
        raise Exception(
            'ARM_GNU_DIR does not exists in file system os is not directory path')
    
    env['ARM_GNU_DIR'] = os.environ['ARM_GNU_DIR']

    return True
        


def _check_debug_flag():
    return ('NDEBUG' not in os.environ
            or ('NDEBUG' in os.environ
                and os.environ['NDEBUG'].lower().strip() in ['no', 'off', 'false', '0']))


def _default_cflags():
    flags = [
        '-pipe',
    ]
    return ' '.join(flags)


def _default_cxxflags():
    flags = [
        '-pipe',
    ]
    return ' '.join(flags)


def _default_linkflags():
    flags = [
        "-Xlinker --gc-sections"
    ]
    return ' '.join(flags)


def generate(env):
    gcc.generate(env)

    _check_tool_path(env)

    prefix_path = os.path.join(env['ARM_GNU_DIR'], 'bin', _COMPILER_PREFIX)

    env.Replace(
        AR=prefix_path + 'ar',
        AS=prefix_path + 'as',
        CC=prefix_path + 'gcc',
        CXX=prefix_path + 'g++',
        LD=prefix_path + 'gcc',
        OBJCOPY=prefix_path + 'objcopy',
        OBJDUMP=prefix_path + 'objdump',
        RANLIB=prefix_path + 'ranlib',
        PROGSUFFIX=".elf",
        LINKCOM='$LINK -o $TARGET $__RPATH $_LIBDIRFLAGS -Wl,--start-group $LINKFLAGS $SOURCES $_LIBFLAGS -Wl,--end-group'
    )

    env.Append(CFLAGS=_default_cflags())
    env.Append(CXXFLAGS=_default_cxxflags())
    env.Append(LINKFLAGS=_default_linkflags())

    _hexify = env.Builder(action=Action("{} -O ihex $SOURCE $TARGET".format(env["OBJCOPY"])),
                          suffix=".hex",
                          src_suffix=".elf")

    _dumper = env.Builder(action=Action("{} -d -S $SOURCE > $TARGET ".format(env["OBJDUMP"])),
                          suffix=".lst",
                          src_suffix=".elf")

    _binify = env.Builder(action=Action("{} -O binary $SOURCE $TARGET".format(env["OBJCOPY"])),
                          suffix=".bin",
                          src_suffix='.elf')

    env.Append(BUILDERS={'Hex': _hexify})
    env.Append(BUILDERS={'DumpList': _dumper})
    env.Append(BUILDERS={'Binary': _binify})

def exists(env):
    return True
