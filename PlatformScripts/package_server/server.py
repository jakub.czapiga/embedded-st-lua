import paho.mqtt.client as mqtt
import re 
import os
import sys

def on_message_received(client, userdata, msg):
    match = re.match(r"^/pkg/get$", msg.topic)

    try:
        if match:
            params = msg.payload.decode("utf-8").split(';')
            if os.path.isfile('./packages/' + params[1] + ".zip"):
                in_file = open("./packages/" + params[1] + ".zip", "rb")
                data = in_file.read()
                in_file.close()

                out_topic = "/dev/" + params[0] + "/pkg/data"

                out = params[1].encode('utf-8') + b';' + data

                client.publish(out_topic, bytearray(out))
            else:
                print("No file: " + './packages/' + params[1] + ".zip")
    except Exception as e:
        print(e)


client = mqtt.Client("pkg_srv")
client.on_message = on_message_received

client.connect(sys.argv[1], int(sys.argv[2]))

client.subscribe('/pkg/get')

packages_string = ";".join([x[:-4] for x in os.listdir('./packages') if x.endswith('.zip')])
client.publish("/pkg/list", packages_string, qos=0, retain=True)

client.loop_forever()