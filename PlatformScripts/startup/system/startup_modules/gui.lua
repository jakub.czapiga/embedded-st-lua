lcd = require "lcd"
ts = require "ts"

gui_module = {}

function gui_module.draw_button(element)
    local x = element.x
    local y = element.y
    local w = element.width
    local h = element.height
    local text = element.text or ""
    local frame_color = element.frame_color or lcd.COLOR_BLACK
    local inner_color = element.inner_color or lcd.COLOR_GREY
    local text_color = element.text_color or lcd.COLOR_BLACK
    
    lcd.set_text_color(frame_color)  
    lcd.draw_rect(x, y, w, h)
    lcd.set_text_color(inner_color)
    lcd.fill_rect(x + 1, y + 1, w - 1, h - 1)

    lcd.set_back_color(inner_color)
    lcd.set_text_color(text_color)

    local text_len = #text

    lcd.display_string_at(x + math.ceil((w - text_len * lcd.get_font_width()) / 2), y + math.ceil((h - lcd.get_font_size()) / 2) + 1, text, lcd.LEFT_MODE)
end

function gui_module.draw_text(element)
    lcd.set_text_color(element.text_color or lcd.COLOR_BLACK)
    lcd.set_back_color(element.back_color or lcd.COLOR_WHITE)
    local text
    if type(element.text) == "string" then
        text = element.text
    else
        text = element.text()
    end
    
    if element.line ~= nil then
        lcd.display_string(element.line, text)
    else
        lcd.display_string_at(element.x, element.y, text, element.align_mode or lcd.LEFT_MODE) 
    end
end

function gui_module.draw_gui(context_list)
    for _, context in ipairs(context_list) do
        for _, element in ipairs(context.elements) do
            if element.class == "button" then
                gui_module.draw_button(element)
            elseif element.class == "function" then
                element.fn()
            elseif element.class == "text" then
                gui_module.draw_text(element)
            end
        end
    end
end

function distance( a, b )
	return math.sqrt( ( b.x - a.x )^2 + ( b.y - a.y )^2 )
end

gui_module.last_touches = {}

function gui_module.process_gui(context_list)
    local touches = ts.get_touches()
    local new_last_touches = {}
    
    for _, last_touch in ipairs(gui_module.last_touches) do
        for i, touch in ipairs(touches) do
            if distance(touch, last_touch) < 10 then
                table.insert(new_last_touches, touch)
                touches[i] = nil
                break
            end
        end
    end
    
    for _, context in ipairs(context_list) do
        for _, element in ipairs(context.elements) do
            if element.class == "button" and element.action ~= nil and not element.was_pressed then
                for _,v in ipairs(touches) do
                    if v.x >= element.x and v.x <= (element.x + element.width) and v.y >= element.y and v.y <= (element.y + element.height) then
                        element.was_pressed = true
                        element.action(element.action_args or {})
                        break
                    end
                end
                element.was_pressed = false
            end
        end
    end
    
    for _,touch in ipairs(touches) do
       table.insert(new_last_touches, touch) 
    end
    
    gui_module.last_touches = new_last_touches
end



return gui_module