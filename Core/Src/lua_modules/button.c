#include "lua_modules/button.h"

#include "lua_modules/common.h"

#include "rotable.h"

#include "stm32746g_discovery.h"

static int init(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 0);
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
    return 0;
}

static int is_pressed(lua_State * L)
{
    lua_modules_expect_exact_arguments_number(L, 0);

    lua_pushboolean(L, BSP_PB_GetState(BUTTON_KEY) != 0);

    return 1;
}

static const luaL_Reg lib[] = {
    {"init", init},
    {"is_pressed", is_pressed},

    {NULL, NULL}};

int luaopen_button(lua_State *L) {
	rotable_newlib(L, lib);
	return 1;
}