#include "lua_modules/touch_screen.h"

#include "rotable.h"

#include "stm32746g_discovery_ts.h"

static int init(lua_State *L)
{
    int x = luaL_checkinteger(L, 1);
    int y = luaL_checkinteger(L, 2);

    lua_pushboolean(L, BSP_TS_Init(x, y) == TS_OK);

    return 1;
}

static int get_touches(lua_State *L)
{
    TS_StateTypeDef state;
    if (BSP_TS_GetState(&state) != TS_OK) {
        lua_createtable(L, 0, 0);
        return 1;
    }
    
    lua_createtable(L, state.touchDetected, 0);

    for (size_t i = 0; i < state.touchDetected; i++)
    {
        lua_createtable(L, 0, 2);
        
        lua_pushinteger(L, state.touchX[i]);
        lua_setfield(L, -2, "x");

        lua_pushinteger(L, state.touchY[i]);
        lua_setfield(L, -2, "y");

        lua_rawseti(L, -2, i + 1);
    }

    return 1;
}

static const luaL_Reg lib[] = {
    {"init", init},
    {"get_touches", get_touches},

    {NULL, NULL}};

int luaopen_ts(lua_State *L)
{
    rotable_newlib(L, lib);
    return 1;
}