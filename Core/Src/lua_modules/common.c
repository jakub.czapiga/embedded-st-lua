#include "lua_modules/common.h"

#include "lua_modules/system.h"
#include "lua_modules/lcd.h"
#include "lua_modules/package_manager.h"
#include "lua_modules/touch_screen.h"
#include "lua_modules/network_interface.h"
#include "lua_modules/button.h"
#include "lua_modules/mqtt_client.h"

int lua_modules_preload_all(lua_State *L)
{
    luaL_getsubtable(L, LUA_REGISTRYINDEX, LUA_PRELOAD_TABLE);

    lua_pushcfunction(L, luaopen_system);
    lua_setfield(L, -2, LUA_MODULES_SYSTEM_LIB_NAME);

    lua_pushcfunction(L, luaopen_lcd);
    lua_setfield(L, -2, LUA_MODULES_LCD_LIB_NAME);

    lua_pushcfunction(L, luaopen_package_manager);
    lua_setfield(L, -2, LUA_MODULES_PACKAGE_MANAGER_LIB_NAME);

    lua_pushcfunction(L, luaopen_ts);
    lua_setfield(L, -2, LUA_MODULES_TOUCH_SCREEN_LIB_NAME);

    lua_pushcfunction(L, luaopen_network_interface);
    lua_setfield(L, -2, LUA_MODULES_NETWORK_INTERFACE_LIB_NAME);

    lua_pushcfunction(L, luaopen_button);
    lua_setfield(L, -2, LUA_MODULES_BUTTON_LIB_NAME);

    lua_pushcfunction(L, luaopen_mqtt_client);
    lua_setfield(L, -2, LUA_MODULES_MQTT_CLIENT_LIB_NAME);

    lua_pop(L, 1); // remove PRELOAD table

    return 0;
}

void lua_modules_setfield_integer(lua_State *L, const char *key, lua_Integer value)
{
    lua_pushinteger(L, value);
    lua_setfield(L, -2, key);
}

int lua_modules_is_arguments_number_in_range(lua_State *L, int a, int b)
{
    return LUA_MODULES_IS_ARGUMENT_NUMBER_IN_RANGE(L, a, b);
}

void lua_modules_expect_arguments_number_in_range(lua_State *L, int a, int b)
{
    LUA_MODULES_EXPECT_ARGUMENTS_NUMBER_IN_RANGE(L, a, b);
}

void lua_modules_expect_exact_arguments_number(lua_State *L, int a)
{
    LUA_MODULES_EXPECT_EXACT_ARGUMENTS_NUMBER(L, a);
}