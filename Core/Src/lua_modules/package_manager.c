#include "lua_modules/package_manager.h"

#include "lauxlib.h"
#include "rotable.h"

#include "package_manager/package_manager.h"

static int init(lua_State *L)
{
    const char * packages_dir = luaL_checkstring(L, 1);
    const char * apps_dir = luaL_checkstring(L, 2);

    const ErrorStatus ret = PackageManager_init(packages_dir, apps_dir);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}

static int get_packages_dir(lua_State*L)
{
    lua_pushstring(L, PackageManager_get_packages_dir());
    return 1;
}

static int get_applications_dir(lua_State*L)
{
    lua_pushstring(L, PackageManager_get_applications_dir());
    return 1;
}

static int set_packages_dir(lua_State*L)
{
    const char * new_packages_dir = luaL_checkstring(L, 1);
    const ErrorStatus ret = PackageManager_set_packages_dir(new_packages_dir);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}


static int set_applications_dir(lua_State*L)
{
    const char * new_applications_dir = luaL_checkstring(L, 1);
    const ErrorStatus ret = PackageManager_set_applications_dir(new_applications_dir);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}


static int unpack_package(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);

    const ErrorStatus ret = PackageManager_unpack_package(app_name);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}


static int remove_package(lua_State*L)
{
    const char * package_name = luaL_checkstring(L, 1);

    const ErrorStatus ret = PackageManager_remove_package(package_name);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}

static int remove_application(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);

    const ErrorStatus ret = PackageManager_remove_application(app_name);

    lua_pushboolean(L, ret == SUCCESS);
    return 1;
}

static int application_is_unpacked(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);

    const int ret = PackageManager_application_is_unpacked(app_name);

    lua_pushboolean(L, ret);
    return 1;
}

static char buffer[256];

static int application_get_working_directory(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);
    buffer[0] = 0;

    const ErrorStatus ret = PackageManager_application_get_working_directory(app_name, buffer, sizeof(buffer));

    if (ret == SUCCESS) {
        lua_pushstring(L, buffer);
    } else {
        lua_pushnil(L);
    }

    return 1;
}

static int application_get_package_location(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);
    buffer[0] = 0;

    const ErrorStatus ret = PackageManager_application_get_package_location(app_name, buffer, sizeof(buffer));

    if (ret == SUCCESS) {
        lua_pushstring(L, buffer);
    } else {
        lua_pushnil(L);
    }

    return 1;
}

static int application_get_main_file_path(lua_State*L)
{
    const char * app_name = luaL_checkstring(L, 1);
    buffer[0] = 0;

    const ErrorStatus ret = PackageManager_application_get_main_file_path(app_name, buffer, sizeof(buffer));

    if (ret == SUCCESS) {
        lua_pushstring(L, buffer);
    } else {
        lua_pushnil(L);
    }

    return 1;
}

static int get_packages_list(lua_State*L)
{
    Array_cstring * arr = PackageManager_get_packages_list();

    if (arr == NULL) {
        lua_createtable(L, 0, 0);
        return 1;
    }

    lua_createtable(L, arr->size, 0);

    cstring str;

    for (size_t i = 0; i < arr->size; ++i) {
        Array_cstring_get(arr, i, &str);

        lua_pushstring(L, str);
        lua_rawseti(L, -2, i + 1);
    }

    PackageManager_free_list(arr);

    return 1;
}

static int get_applications_list(lua_State*L)
{
    lua_newtable(L);

    Array_cstring * arr = PackageManager_get_applications_list();

    if (arr == NULL) {
        lua_createtable(L, 0, 0);
        return 1;
    }

    lua_createtable(L, arr->size, 0);

    cstring str;

    for (size_t i = 0; i < arr->size; ++i) {
        Array_cstring_get(arr, i, &str);

        lua_pushstring(L, str);
        lua_rawseti(L, -2, i + 1);
    }

    PackageManager_free_list(arr);

    return 1;
}

static const luaL_Reg lib[] = {
    {"init", init},
    {"get_packages_dir", get_packages_dir},
    {"get_applications_dir", get_applications_dir},
    {"set_packages_dir", set_packages_dir},
    {"set_applications_dir", set_applications_dir},
    {"unpack_package", unpack_package},
    {"remove_package", remove_package},
    {"remove_application", remove_application},
    {"application_is_unpacked", application_is_unpacked},
    {"application_get_working_directory", application_get_working_directory},
    {"application_get_package_location", application_get_package_location},
    {"application_get_main_file_path", application_get_main_file_path},
    {"get_packages_list", get_packages_list},
    {"get_applications_list", get_applications_list},

    {NULL, NULL}};

int luaopen_package_manager(lua_State *L)
{
    rotable_newlib(L, lib);
    return 1;
}