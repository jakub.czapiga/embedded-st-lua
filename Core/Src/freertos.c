/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include <string.h>
#include <stdio.h>
#include <setjmp.h>

#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_uart.h"
#include "stm32746g_discovery.h"
#include "usart.h"
#include "fatfs.h"

#include "lua_modules/common.h"
#include "lua_modules/system.h"

#include "ansi-colors.h"

#include "package_manager/package_manager.h"

#include "lstate.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId mainTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void startMainTask(void const * argument);

extern void MX_FATFS_Init(void);
extern void MX_LWIP_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationIdleHook(void);
void vApplicationTickHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 2 */
__weak void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */
}
/* USER CODE END 2 */

/* USER CODE BEGIN 3 */
__weak void vApplicationTickHook( void )
{
   /* This function will be called by each tick interrupt if
   configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h. User code can be
   added here, but the tick hook is called from an interrupt context, so
   code must not attempt to block, and only the interrupt safe FreeRTOS API
   functions can be used (those that end in FromISR()). */
}
/* USER CODE END 3 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
  printf("Stack overflow in task: %s\n", pcTaskName);
  while(1) {

  }
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
  printf("Malloc failed\n");
  while(1) {
    
  }
}
/* USER CODE END 5 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of mainTask */
  osThreadDef(mainTask, startMainTask, osPriorityNormal, 0, 4096);
  mainTaskHandle = osThreadCreate(osThread(mainTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}


static void *freertos_lua_alloc(void *ud, void *ptr, size_t osize, size_t nsize)
{
  (void)ud;
  (void)osize;
  if (nsize == 0)
  {
    vPortFree(ptr);
    return NULL;
  }
  else
  {
    return pvPortRealloc(ptr, nsize);
  }
}

/* USER CODE BEGIN Header_startMainTask */
/**
  * @brief  Function implementing the mainTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_startMainTask */
__weak void startMainTask(void const * argument)
{     
  /* init code for FATFS */
  MX_FATFS_Init();

  /* USER CODE BEGIN startMainTask */
  puts(ANSI_COLOR_GREEN "\nPlatform starting" ANSI_RESET);

  lua_State * L = NULL;

  if (setjmp(system_termination_buffer) == 0 || selected_application == NULL)
  {
    const char *path = "/sd/startup.lua";

    if (L)
    {
      lua_close(L);
    }

    L = lua_newstate(freertos_lua_alloc, 0);
    G(L)->mutex = osMutexCreate(NULL);

    luaL_openlibs(L);
    lua_modules_preload_all(L);

    if (luaL_loadfile(L, path))
    {
      printf("[ERROR] Failed to load file:  %s\n", lua_tostring(L, -1));
    }
    else
    {
      if (lua_pcall(L, 0, LUA_MULTRET, 0))
      {
        printf("[ERROR] %s\n", lua_tostring(L, -1));
      }
    }

    lua_close(L);
    osMutexDelete(G(L)->mutex);
    L = NULL;
  }
  else
  {
    if (L)
    {
      lua_close(L);
      osMutexDelete(G(L)->mutex);
      L = NULL;
    }

    char buffer[256] = {0};

    PackageManager_application_get_working_directory(selected_application, buffer, sizeof(buffer));
    f_chdir(buffer);

    buffer[0] = 0;
    PackageManager_application_get_main_file_path(selected_application, buffer, sizeof(buffer));

    L = lua_newstate(freertos_lua_alloc, 0);
    G(L)->mutex = osMutexCreate(NULL);

    luaL_openlibs(L);
    lua_modules_preload_all(L);

    if (luaL_loadfile(L, buffer))
    {
      printf("[ERROR] Failed to load file:  %s\n", lua_tostring(L, -1));
    }
    else
    {
      if (lua_pcall(L, 0, LUA_MULTRET, 0))
      {
        printf("[ERROR] %s\n", lua_tostring(L, -1));
      }
    }

    lua_close(L);
    osMutexDelete(G(L)->mutex);
    L = NULL;
  }

  BSP_LED_Init(LED1);

  for (;;)
  {
    BSP_LED_Toggle(LED1);
    osDelay(1000);
  }
  /* USER CODE END startMainTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
