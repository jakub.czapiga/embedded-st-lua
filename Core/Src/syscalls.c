/**
*****************************************************************************
**
**  File        : syscalls.c
**
**  Abstract    : System Workbench Minimal System calls file
**
** 		          For more information about which c-functions
**                need which of these lowlevel functions
**                please consult the Newlib libc-manual
**
**  Environment : System Workbench for MCU
**
**  Distribution: The file is distributed “as is,” without any warranty
**                of any kind.
**
*****************************************************************************
**
** <h2><center>&copy; COPYRIGHT(c) 2014 Ac6</center></h2>
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**   1. Redistributions of source code must retain the above copyright notice,
**      this list of conditions and the following disclaimer.
**   2. Redistributions in binary form must reproduce the above copyright notice,
**      this list of conditions and the following disclaimer in the documentation
**      and/or other materials provided with the distribution.
**   3. Neither the name of Ac6 nor the names of its contributors
**      may be used to endorse or promote products derived from this software
**      without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*****************************************************************************
*/

/* Includes */
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/times.h>

#include <fcntl.h>
#include "cmsis_os.h"
#include "fatfs.h"
#include "unistd.h"
#include "FreeRTOS.h"
#include "usart.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_uart.h"
/* Variables */
//#undef errno
extern int errno;
extern int __io_putchar(int ch) __attribute__((weak));
extern int __io_getchar(void) __attribute__((weak));

register char * stack_ptr asm("sp");

char *__env[1] = { 0 };
char **environ = __env;

#define MAX_OPEN_FILES 8

#define FILE_DESCRIPTOR_BASE 1001

typedef struct {
	short descriptor;
	union {
		FIL * fil_ptr;
	} handler;
} FileDescriptorRecord_t;

FileDescriptorRecord_t fileDescriptorMapTable[MAX_OPEN_FILES] = {0};

SemaphoreHandle_t fileDescriptorMapTableMutex = NULL;
SemaphoreHandle_t fileDescriptorMapTableAvailableEntriesSemaphore = NULL;

#define fileDescriptorToMapTableIndex(d) ((d) - FILE_DESCRIPTOR_BASE)

static int obtainFirstFreeDescriptorTableEntry() {
	int ret = -1;
	if(xSemaphoreTake(fileDescriptorMapTableAvailableEntriesSemaphore, 0) == pdFALSE) {
		return -1;
	}

	while(xSemaphoreTake(fileDescriptorMapTableMutex, portMAX_DELAY) != pdTRUE) {
		;
	}

	for(int i = 0; i < MAX_OPEN_FILES; ++i) {
		if (fileDescriptorMapTable[i].descriptor == 0) {
			ret = i;
			goto cleanup;
		}
	}

	xSemaphoreGive(fileDescriptorMapTableAvailableEntriesSemaphore);
cleanup:
	xSemaphoreGive(fileDescriptorMapTableMutex);

	return ret;
}

static void freeDescriptorTableEntry(int no) {
	if(fileDescriptorMapTable[no].descriptor != 0) {
		while(xSemaphoreTake(fileDescriptorMapTableMutex, portMAX_DELAY) != pdTRUE) {
			;
		}
		{
			fileDescriptorMapTable[no].descriptor = 0;
			fileDescriptorMapTable[no].handler.fil_ptr = NULL;
		}
		xSemaphoreGive(fileDescriptorMapTableMutex);


		xSemaphoreGive(fileDescriptorMapTableAvailableEntriesSemaphore);
	}
}

/* Functions */
void initialise_monitor_handles(void)
{
	for (int i = 0; i < MAX_OPEN_FILES; ++i)
	{
		fileDescriptorMapTable[i].descriptor = 0;
		fileDescriptorMapTable[i].handler.fil_ptr = NULL;
	}

	fileDescriptorMapTableMutex = xSemaphoreCreateMutex();
	xSemaphoreGive(fileDescriptorMapTableMutex);

	fileDescriptorMapTableAvailableEntriesSemaphore = xSemaphoreCreateCounting(MAX_OPEN_FILES, MAX_OPEN_FILES);
}

int _getpid(void)
{
	return 1;
}

int _kill(int pid, int sig)
{
	errno = EINVAL;
	return -1;
}

void _exit (int status)
{
	_kill(status, -1);
	while (1) {}		/* Make sure we hang here */
}

int _read(int file, char *ptr, int len)
{
	if (file == STDIN_FILENO)
	{
		int DataIdx;

		for (DataIdx = 0; DataIdx < len; DataIdx++)
		{
			*ptr++ = __io_getchar();
		}

		return len;
	}
	else
	{
		if (ptr == NULL || len == 0)
		{
			return 0;
		}

		if (!(file >= FILE_DESCRIPTOR_BASE && file <= FILE_DESCRIPTOR_BASE + MAX_OPEN_FILES))
		{
			return -1;
		}

		int descriptorTableIndex = fileDescriptorToMapTableIndex(file);

		if (fileDescriptorMapTable[descriptorTableIndex].descriptor != file)
		{
			return -1;
		}

		FIL *fil = fileDescriptorMapTable[descriptorTableIndex].handler.fil_ptr;

		UINT br = 0;

		if (f_read(fil, ptr, len, &br) != FR_OK)
		{
			return -1;
		}

		return br;
	}
	return -1;
}

int _write(int file, char *ptr, int len)
{
	if (file == STDOUT_FILENO || file == STDERR_FILENO)
	{
		HAL_UART_Transmit(&huart1, (uint8_t*)ptr, len, 0xFFFFFFFF);

		return len;
	}
	else if (file == STDIN_FILENO)
	{
		return -1;
	}
	else
	{
		if (!(file >= FILE_DESCRIPTOR_BASE && file <= FILE_DESCRIPTOR_BASE + MAX_OPEN_FILES))
		{
			return -1;
		}

		int descriptorTableIndex = fileDescriptorToMapTableIndex(file);

		if (fileDescriptorMapTable[descriptorTableIndex].descriptor != file)
		{
			return -1;
		}

		FIL *fil = fileDescriptorMapTable[descriptorTableIndex].handler.fil_ptr;
		UINT bw = 0;

		if (f_write(fil, ptr, len, &bw) != FR_OK)
		{
			extern void Error_Handler();
			Error_Handler();
			return 0;
		}

		return bw;
	}
}

caddr_t _sbrk(int incr)
{
	extern char end asm("end");
	static char *heap_end;
	char *prev_heap_end,*min_stack_ptr;

	if (heap_end == 0)
		heap_end = &end;

	prev_heap_end = heap_end;

	/* Use the NVIC offset register to locate the main stack pointer. */
	min_stack_ptr = (char*)(*(unsigned int *)*(unsigned int *)0xE000ED08);
	/* Locate the STACK bottom address */
	extern unsigned long long _Min_Stack_Size;
	min_stack_ptr -= (char*)(&_Min_Stack_Size);

	if (heap_end + incr > min_stack_ptr)
	{
		errno = ENOMEM;
		return (caddr_t) -1;
	}

	heap_end += incr;

	return (caddr_t) prev_heap_end;
}

int _close(int file)
{
	if (file != STDIN_FILENO && file != STDOUT_FILENO && file != STDERR_FILENO)
	{
		if (!(file >= FILE_DESCRIPTOR_BASE && file <= FILE_DESCRIPTOR_BASE + MAX_OPEN_FILES))
		{
			return -1;
		}

		int descriptorTableIndex = fileDescriptorToMapTableIndex(file);

		if (fileDescriptorMapTable[descriptorTableIndex].descriptor != file)
		{
			return -1;
		}

		FIL *fil = fileDescriptorMapTable[descriptorTableIndex].handler.fil_ptr;

		if (f_close(fil) != FR_OK)
		{
			return -1;
		}
		else
		{
			freeDescriptorTableEntry(descriptorTableIndex);
			vPortFree(fil);
			return 0;
		}
	}
	return 0;
}

int _fstat(int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(int file)
{
	return 1;
}

int _lseek(int file, int ptr, int dir)
{
	if (!(file >= FILE_DESCRIPTOR_BASE && file <= FILE_DESCRIPTOR_BASE + MAX_OPEN_FILES))
	{
		return -1;
	}

	int descriptorTableIndex = fileDescriptorToMapTableIndex(file);

	if (fileDescriptorMapTable[descriptorTableIndex].descriptor!= file)
	{
		return -1;
	}

	FIL *fil = fileDescriptorMapTable[descriptorTableIndex].handler.fil_ptr;
	FSIZE_t pos = 0;

	if (dir == SEEK_SET)
	{
		pos = ptr;
	}
	else if (dir == SEEK_CUR)
	{
		pos = f_tell(fil) + ptr;
	}
	else if (dir == SEEK_END)
	{
		pos = f_size(fil);
	}
	else
	{
		errno = EINVAL;
		return -1;
	}

	if ( !(f_lseek(fil, pos) == FR_OK)) {
		return -1;
	}

	return f_tell(fil);
}

int _open(char *path, int flags, ...)
{
	FIL *fil = NULL;
	BYTE mode = 0;
	int descriptorTableIndex = -1;

	descriptorTableIndex = obtainFirstFreeDescriptorTableEntry();

	if (descriptorTableIndex == -1)
	{
		errno = ENFILE;
		return -1;
	}

	fil = pvPortMalloc(sizeof(FIL));

	if (fil == NULL)
	{
		errno = ENOMEM;
		freeDescriptorTableEntry(descriptorTableIndex);
		return -1;
	}

#ifdef O_BINARY
	flags &= ~O_BINARY;
#endif

	if (((flags & (O_CREAT | O_TRUNC)) == (O_CREAT | O_TRUNC)) && (flags & (O_RDWR | O_WRONLY)))
		mode = FA_CREATE_ALWAYS;
	else if ((flags & (O_CREAT | O_EXCL)) == (O_CREAT | O_EXCL))
		mode = FA_OPEN_EXISTING;
	else if ((flags & O_CREAT) == O_CREAT)
		mode = FA_OPEN_ALWAYS;
	else if ((flags == O_RDONLY) || (flags == O_WRONLY) || (flags == O_RDWR))
		mode = FA_OPEN_EXISTING;
	else
	{
		errno = ENOSYS;
		freeDescriptorTableEntry(descriptorTableIndex);
		return -1;
	}

	if (flags & O_APPEND)
	{
		mode |= FA_OPEN_APPEND;
	}

	if ((flags & O_ACCMODE) == O_RDONLY)
		mode |= FA_READ;
	else if ((flags & O_ACCMODE) == O_WRONLY)
		mode |= FA_WRITE;
	else if ((flags & O_ACCMODE) == O_RDWR)
		mode |= (FA_READ | FA_WRITE);
	else
	{
		errno = ENOSYS;
		freeDescriptorTableEntry(descriptorTableIndex);
		return -1;
	}

	FRESULT fr = f_open(fil, path, mode);

	if (fr != FR_OK)
	{
		switch(fr) {
		case FR_NO_FILE:
			errno = ENOENT;
			break;
		case FR_EXIST:
			errno = EEXIST;
			break;
		case FR_TOO_MANY_OPEN_FILES:
			errno = EMFILE;
			break;
		case FR_TIMEOUT:
			errno = ETXTBSY;
			break;
		case FR_WRITE_PROTECTED:
			errno = EROFS;
			break;
		default:
			errno = ENOSYS;
		}
		freeDescriptorTableEntry(descriptorTableIndex);
		return -1;
	}

	fileDescriptorMapTable[descriptorTableIndex].descriptor = descriptorTableIndex + FILE_DESCRIPTOR_BASE;
	fileDescriptorMapTable[descriptorTableIndex].handler.fil_ptr = fil;

	return fileDescriptorMapTable[descriptorTableIndex].descriptor;
}

int _wait(int *status)
{
	errno = ECHILD;
	return -1;
}

int _unlink(char *name)
{
	errno = ENOENT;
	return -1;
}

int _times(struct tms *buf)
{
	return -1;
}

int _stat(char *file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _link(char *old, char *new)
{
	errno = EMLINK;
	return -1;
}

int _fork(void)
{
	errno = EAGAIN;
	return -1;
}

int _execve(char *name, char **argv, char **env)
{
	errno = ENOMEM;
	return -1;
}

int _gettimeofday (struct timeval* tp, struct timezone* tzp) {
  /* Return fixed data for the timezone.  */
  if (tzp) {
    tzp->tz_minuteswest = 0;
    tzp->tz_dsttime = 0;
  }

  return 0;
}