#ifndef LUA_MODULES_TOUCH_SCREEN_H
#define LUA_MODULES_TOUCH_SCREEN_H

#include "lua_modules/common.h"

#define LUA_MODULES_TOUCH_SCREEN_LIB_NAME "ts"

int luaopen_ts(lua_State *L);


#endif /* LUA_MODULES_TOUCH_SCREEN_H */