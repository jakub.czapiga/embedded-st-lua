#ifndef LUA_MODULES_SYSTEM_H
#define LUA_MODULES_SYSTEM_H

#include <setjmp.h>

#include "lua_modules/common.h"

#define LUA_MODULES_SYSTEM_LIB_NAME "system"

extern jmp_buf system_termination_buffer;
extern char * selected_application;

int luaopen_system(lua_State *L);

#endif /* LUA_MODULES_SYSTEM_H */
