#ifndef LUA_MODULES_BUTTON_H
#define LUA_MODULES_BUTTON_H

#include "lua_modules/common.h"

#define LUA_MODULES_BUTTON_LIB_NAME "button"

int luaopen_button(lua_State *L);

#endif /* LUA_MODULES_BUTTON_H */