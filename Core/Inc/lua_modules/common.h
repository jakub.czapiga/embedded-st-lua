#ifndef LUA_MODULES_COMMON_H
#define LUA_MODULES_COMMON_H

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

int lua_modules_preload_all(lua_State *L);

void lua_modules_setfield_integer(lua_State *L, const char *key, lua_Integer v);

int lua_modules_is_arguments_number_in_range(lua_State *L, int a, int b);

void lua_modules_expect_arguments_number_in_range(lua_State *L, int a, int b);

void lua_modules_expect_exact_arguments_number(lua_State *L, int a);

#define LUA_MODULES_IS_ARGUMENT_NUMBER_IN_RANGE(L, a, b) ((a) <= lua_gettop((L)) && lua_gettop((L)) <= (b))

#define LUA_MODULES_EXPECT_ARGUMENTS_NUMBER_IN_RANGE(L, a, b)                                   \
    do                                                                                          \
    {                                                                                           \
        if (!LUA_MODULES_IS_ARGUMENT_NUMBER_IN_RANGE(L, a, b))                                   \
        {                                                                                       \
            if (a == b)                                                                         \
            {                                                                                   \
                luaL_error(L, "expected %d arguments, got %d", a, lua_gettop(L));               \
            }                                                                                   \
            else                                                                                \
            {                                                                                   \
                luaL_error(L, "expected between %d to %d arguments, got %d", a, lua_gettop(L)); \
            }                                                                                   \
        }                                                                                       \
    } while (0)

#define LUA_MODULES_EXPECT_EXACT_ARGUMENTS_NUMBER(L, a)                                    \
    do                                                                                     \
    {                                                                                      \
        if (lua_gettop(L) != a)                                                            \
        {                                                                                  \
            luaL_error(L, "this function expects %d arguments, got %d", a, lua_gettop(L)); \
        }                                                                                  \
    } while (0)

#endif /* LUA_MODULES_COMMON_H */