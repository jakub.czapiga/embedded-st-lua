#ifndef LUA_MODULES_MQTT_CLIENT_H
#define LUA_MODULES_MQTT_CLIENT_H

#include "lua_modules/common.h"

#define LUA_MODULES_MQTT_CLIENT_LIB_NAME "mqtt_client"

int luaopen_mqtt_client(lua_State *L);

#endif /* LUA_MODULES_MQTT_CLIENT_H */