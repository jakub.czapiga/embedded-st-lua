#ifndef LIB_ARRAY_H
#define LIB_ARRAY_H

#include <stddef.h>
#include <stdint.h>

typedef char *cstring;

#define Array_decl(Type)                                                           \
    typedef struct array_##Type##_t                                                \
    {                                                                              \
        size_t size;                                                               \
        Type *elements;                                                            \
    } Array_##Type;                                                                \
                                                                                   \
    Array_##Type *Array_##Type##_new(size_t size);                                 \
                                                                                   \
    void Array_##Type##_delete(Array_##Type *array);                               \
                                                                                   \
    int Array_##Type##_get(const Array_##Type *array, size_t index, Type *output); \
                                                                                   \
    int Array_##Type##_set(Array_##Type *array, size_t index, Type *contents);     \
                                                                                   \
    int Array_##Type##_append(Array_##Type *array, Type *contents);

#define Array_impl(Type)                                                             \
    Array_##Type *Array_##Type##_new(size_t size)                                    \
    {                                                                                \
        Array_##Type *newArray = (Array_##Type *)pvPortMalloc(sizeof(Array_##Type)); \
                                                                                     \
        if (!newArray)                                                               \
        {                                                                            \
            return NULL;                                                             \
        }                                                                            \
                                                                                     \
        if (size != 0)                                                               \
        {                                                                            \
            Type *const data_ptr = (Type *)pvPortMalloc(size * sizeof(Type));        \
                                                                                     \
            if (!data_ptr)                                                           \
            {                                                                        \
                vPortFree(newArray);                                                 \
                return NULL;                                                         \
            }                                                                        \
            newArray->elements = data_ptr;                                           \
        }                                                                            \
        else                                                                         \
        {                                                                            \
            newArray->elements = NULL;                                               \
        }                                                                            \
                                                                                     \
        newArray->size = size;                                                       \
                                                                                     \
        return newArray;                                                             \
    }                                                                                \
                                                                                     \
    void Array_##Type##_delete(Array_##Type *array)                                  \
    {                                                                                \
        if (!array)                                                                  \
        {                                                                            \
            return;                                                                  \
        }                                                                            \
                                                                                     \
        if (array->elements)                                                         \
        {                                                                            \
            vPortFree(array->elements);                                              \
        }                                                                            \
        vPortFree(array);                                                            \
    }                                                                                \
                                                                                     \
    int Array_##Type##_get(const Array_##Type *array, size_t index, Type *output)    \
    {                                                                                \
        if (!array || array->size <= index)                                          \
        {                                                                            \
            return 0;                                                                \
        }                                                                            \
                                                                                     \
        memcpy(output, &(array->elements[index]), sizeof(Type));                     \
                                                                                     \
        return 1;                                                                    \
    }                                                                                \
                                                                                     \
    int Array_##Type##_set(Array_##Type *array, size_t index, Type *contents)        \
    {                                                                                \
        if (!array || array->size <= index)                                          \
        {                                                                            \
            return 0;                                                                \
        }                                                                            \
                                                                                     \
        memcpy(&(array->elements[index]), contents, sizeof(Type));                   \
                                                                                     \
        return 1;                                                                    \
    }                                                                                \
                                                                                     \
    int Array_##Type##_append(Array_##Type *array, Type *contents)                   \
    {                                                                                \
        if (!array)                                                                  \
        {                                                                            \
            return 0;                                                                \
        }                                                                            \
                                                                                     \
        const size_t new_size = array->size + 1;                                     \
        Type *const old_buffer = array->elements;                                    \
                                                                                     \
        Type *new_buffer = NULL;                                                     \
                                                                                     \
        if (old_buffer == NULL && array->size == 0)                                  \
        {                                                                            \
            new_buffer = pvPortCalloc(1, sizeof(Type));                              \
        }                                                                            \
        else if (array->size != 0)                                                   \
        {                                                                            \
            new_buffer = (Type *)pvPortRealloc(old_buffer, new_size * sizeof(Type)); \
        }                                                                            \
        else                                                                         \
        {                                                                            \
            return 0;                                                                \
        }                                                                            \
                                                                                     \
        if (new_buffer != NULL)                                                      \
        {                                                                            \
            array->size = new_size;                                                  \
            array->elements = new_buffer;                                            \
                                                                                     \
            return Array_##Type##_set(array, new_size - 1, contents);                \
        }                                                                            \
                                                                                     \
        return 0;                                                                    \
    }

Array_decl(cstring)

#endif /* LIB_ARRAY_H */