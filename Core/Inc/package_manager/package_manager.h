#ifndef PACKAGE_MANAGER_H
#define PACKAGE_MANAGER_H

#include "stm32f7xx.h"

#include "lib/array.h"

ErrorStatus PackageManager_init(const char *new_packages_dir, const char *new_applications_dir);

const char * PackageManager_get_packages_dir(void);

const char * PackageManager_get_applications_dir(void);

ErrorStatus PackageManager_set_packages_dir(const char *new_packages_dir);

ErrorStatus PackageManager_set_applications_dir(const char *new_applications_dir);

ErrorStatus PackageManager_unpack_package(const char *application_name);

ErrorStatus PackageManager_remove_package(const char *package_name);

ErrorStatus PackageManager_remove_application(const char *application_name);

int PackageManager_application_is_unpacked(const char *application_name);

ErrorStatus PackageManager_application_get_working_directory(const char *application_name, char * buffer, size_t buffer_size);

ErrorStatus PackageManager_application_get_package_location(const char *application_name, char * buffer, size_t buffer_size);

ErrorStatus PackageManager_application_get_main_file_path(const char * application_name, char * buffer, size_t buffer_size);

Array_cstring *PackageManager_get_packages_list(void);

Array_cstring *PackageManager_get_applications_list(void);

void PackageManager_free_list(Array_cstring *ptr);

#endif /* PACKAGE_MANAGER_H */